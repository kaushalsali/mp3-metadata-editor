import id3edit
import sys
from PyQt5 import QtCore, QtWidgets
from editor_ui import Ui_MainWindow

class StartQT5(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self, parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        self.file_name = None
        self.frame_name = None
        self.frame_location = None
        self.music_file = None
        self.music_file_write = None
        self.field_dict = {'Title': 'TIT2', 'Artist': 'TPE1', 'Album': 'TALB', 'Track Number': 'TRCK', 'Composer': 'TCOM', 'Genre': 'TCON', 'Album Artist':'TPE@'}
        self.ui.comboBox.addItems(['Album', 'Album Artist', 'Artist', 'Composer', 'Genre', 'Title', 'Track Number'])

        self.ui.button_open.clicked.connect(self.file_open)
        self.ui.button_save.clicked.connect(self.save_changes)
        self.ui.button_exit.clicked.connect(self.close)
        self.ui.comboBox.currentTextChanged.connect(self.get_field)


    def file_open(self):
        fd = QtWidgets.QFileDialog(self)
        self.file_name = fd.getOpenFileName()[0]
        from os.path import isfile
        if isfile(self.file_name):
            self.music_file = open(self.file_name, 'rb+')
            self.music_file_write = open(self.file_name, 'rb+')
        self.ui.label_filename.setText(self.file_name)

    def get_field(self):
        self.frame_name = self.ui.comboBox.currentText()
        print(self.field_dict[self.frame_name])
        self.frame_location = id3edit.findFrame(self.field_dict[self.frame_name], self.music_file)
        try:
            entry = id3edit.readFrame(self.frame_location, self.music_file)[2]
            self.ui.textEdit_current.setText(str(entry,'utf-8'))
        except TypeError:
            print("frame not found")
            self.ui.textEdit_current.setText("")

    def save_changes(self):
        new_entry = self.ui.textEdit_new.toPlainText().strip()
        print("asdasdasdasd-----> "+new_entry)
        id3edit.editFrame(new_entry, self.frame_location, self.music_file, self.music_file_write)
        self.get_field()


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    myapp = StartQT5()
    myapp.show()
    sys.exit(app.exec_())