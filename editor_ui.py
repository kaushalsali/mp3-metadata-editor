# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'editor_ui.ui'
#
# Created by: PyQt5 UI code generator 5.4.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(1005, 617)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.button_open = QtWidgets.QPushButton(self.centralwidget)
        self.button_open.setGeometry(QtCore.QRect(70, 80, 98, 27))
        self.button_open.setObjectName("button_open")
        self.textEdit_new = QtWidgets.QTextEdit(self.centralwidget)
        self.textEdit_new.setGeometry(QtCore.QRect(20, 420, 961, 81))
        self.textEdit_new.setObjectName("textEdit_new")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(30, 250, 66, 17))
        self.label.setObjectName("label")
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setGeometry(QtCore.QRect(30, 390, 66, 17))
        self.label_2.setObjectName("label_2")
        self.label_3 = QtWidgets.QLabel(self.centralwidget)
        self.label_3.setGeometry(QtCore.QRect(280, 30, 66, 31))
        self.label_3.setObjectName("label_3")
        self.textEdit_current = QtWidgets.QTextEdit(self.centralwidget)
        self.textEdit_current.setGeometry(QtCore.QRect(20, 280, 961, 81))
        self.textEdit_current.setReadOnly(True)
        self.textEdit_current.setObjectName("textEdit_current")
        self.label_filename = QtWidgets.QLabel(self.centralwidget)
        self.label_filename.setGeometry(QtCore.QRect(360, 30, 471, 31))
        self.label_filename.setObjectName("label_filename")
        self.comboBox = QtWidgets.QComboBox(self.centralwidget)
        self.comboBox.setGeometry(QtCore.QRect(360, 144, 291, 27))
        self.comboBox.setObjectName("comboBox")
        self.label_5 = QtWidgets.QLabel(self.centralwidget)
        self.label_5.setGeometry(QtCore.QRect(270, 140, 66, 31))
        self.label_5.setObjectName("label_5")
        self.button_save = QtWidgets.QPushButton(self.centralwidget)
        self.button_save.setGeometry(QtCore.QRect(180, 530, 98, 27))
        self.button_save.setObjectName("button_save")
        self.button_exit = QtWidgets.QPushButton(self.centralwidget)
        self.button_exit.setGeometry(QtCore.QRect(770, 530, 98, 27))
        self.button_exit.setObjectName("button_exit")
        self.label_image = QtWidgets.QLabel(self.centralwidget)
        self.label_image.setGeometry(QtCore.QRect(780, 20, 201, 241))
        self.label_image.setText("")
        self.label_image.setPixmap(QtGui.QPixmap("icon.png"))
        self.label_image.setObjectName("label_image")
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 1005, 25))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "ID3 Tag Editor"))
        self.button_open.setText(_translate("MainWindow", "Open"))
        self.label.setText(_translate("MainWindow", "Current:"))
        self.label_2.setText(_translate("MainWindow", "New:"))
        self.label_3.setText(_translate("MainWindow", "File:"))
        self.label_filename.setText(_translate("MainWindow", "Filename"))
        self.label_5.setText(_translate("MainWindow", "Field:"))
        self.button_save.setText(_translate("MainWindow", "Save"))
        self.button_exit.setText(_translate("MainWindow", "Exit"))

