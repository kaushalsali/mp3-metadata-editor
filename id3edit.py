import struct,sys

#mp3file1 = open(sys.argv[1],'rb+')
#mp3file2 = open(sys.argv[1],'rb+')

#mp3file1.seek(6) # skip tag header

#max_tag_size = struct.unpack('4B',mp3file1.read(4))[3]
#print("Size: "+str(max_tag_size))




# searches for the frame and returns start location  of the frame
def findFrame(frame_name, music_file):
    print("\n-----findFrame------")
    try:
        mp3file1 = music_file
       # global mp3file1,mp3file2
        mp3file1.seek(10) # skip tag header
        notfound=True
        while(notfound):
            frame_start_pos = mp3file1.tell() #stores beginning of frame
            frame = mp3file1.read(4)
            print("Frame: " + str(frame,'utf-8'))
            size = struct.unpack('4B',mp3file1.read(4))[3]  #converts the \x00\x04.. to a tuple (0,4,..)
            print("Size: "+str(size))
            flags = mp3file1.read(2)
    #        print("Flags: " + str(flags,'utf-8'))
            if (str(frame,'utf-8') == frame_name):
                print("----frame found")
                notfound=False
                return (frame_start_pos)
            elif ( mp3file1.tell() > 2000):  #max tag size-----<<<<--!!!!
                return None
            else:
                mp3file1.seek(size,1)
    except AttributeError:
        print("Invalid file name")



def editFrame(new_entry, frame_location, music_file, music_file_write):
    try:
        mp3file1 = music_file
        mp3file2 = music_file_write
        print("\n-----editFrame------")
    # get old size
        mp3file1.seek(frame_location + 4)
        size = struct.unpack('4B',mp3file1.read(4))[3]
    # get new data
        new_data = new_entry
        mp3file1.seek(frame_location + 4)
    # write new data
        mp3file1.write(struct.pack('>i',len(new_data)+1)) # pack & write size of new_data as big endian int (4bytes)
        mp3file1.seek(3,1)   # skip flags + 1 byte (?)
        mp3file2.seek(mp3file1.tell()+size-1)   # 1 byte (?)
        copy_buffer = mp3file2.read()
        mp3file1.write(new_data.encode('utf-8') + copy_buffer)
        print("new --- : "+str(new_data))
    # read new size
        mp3file1.seek(frame_location + 4)
        size = mp3file1.read(4)
        size = struct.unpack('4B',size)[3]
        print("new size : "+str(size))
    # print new data
        mp3file1.seek(2,1) # skip flags
        title = mp3file1.read(size)
        print("new entry: ",end='')
        print(str(title,'utf-8'))
    except AttributeError:
        print("Invalid file name")



def readFrame(frame_location,music_file):
    try:
        mp3file1 = music_file
        mp3file1.seek(frame_location)
        frame = mp3file1.read(4)
        print("Frame: " + str(frame,'utf-8'))
        size = struct.unpack('4B',mp3file1.read(4))[3]
        print("Size: " + str(size))
        mp3file1.seek(2,1) # skip flags
        print("Current Entry: ",end='') # read entry
        entry = mp3file1.read(size)
        print(str(entry,'utf-8'))
        return(frame,size,entry)
    except AttributeError:
        print("Invalid file name")


def printFrameData(frame_location,music_file):
    try:
        print("\n-----printFrameData------")
        mp3file1 = music_file
        mp3file1.seek(frame_location + 4)
        size = struct.unpack('4B',mp3file1.read(4))[3]
        print("Size: " + str(size))
        mp3file1.seek(2,1) # skip flags
    # read entry
        print("Current Entry: ",end='')
        entry = mp3file1.read(size)
        print(str(entry,'utf-8'))
    except AttributeError:
        print("Invalid file name")



def printFrameInfo(frame_location,music_file):
    try:
        mp3file1 = music_file
        print("\n-----printFrameInfo------")
        mp3file1.seek(frame_location)
        frame = mp3file1.read(4)
        print("Frame: " + str(frame,'utf-8'))
        size = struct.unpack('4B',mp3file1.read(4))[3]  #converts the \x00\x04.. to a tuple (0,4,..)
        print("Size: "+str(size))
        flags = mp3file1.read(2)
    #    print("Flags: " + str(flags,'utf-8'))
    except AttributeError:
        print("Invalid file name")

        



'''
#-------------execution----------------------------------------------------------------
a = findFrame(sys.argv[2])
print(a)

printFrameInfo(a)
printFrameData(a)
editFrame(a)
printFrameInfo(a)
printFrameData(a)

mp3file1.close()
mp3file2.close()


#--------------------------------------------------------------------------------------
'''